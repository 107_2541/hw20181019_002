package edu.cyut.javaclass;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.print("請輸入一個分數：");
        Scanner scanner = new Scanner(System.in);
        int score = scanner.nextInt();
        int a = (int) score/10;

        switch (a) {
            case 10:
            case 9:
                System.out.println("得A");
                break;
            case 8:
                System.out.println("得B");
                break;
            case 7:
                System.out.println("得C");
                break;
            case 6:
                System.out.println("得D");
                break;
            default:
                System.out.println("得E(不及格)");
        }

        // write your code here
    }
}
